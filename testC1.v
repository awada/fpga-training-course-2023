`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/08/02 16:00:41
// Design Name: 
// Module Name: testC1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testC1(
    input [3:0] I,
    output [7:0] AN,
    output CA,
    output CB,
    output CC,
    output CD,
    output CE,
    output CF,
    output CG
    );
    
    assign AN[7:0] = 8'b1111_1110;
    
    assign CA = (~I[3] & ~I[2] & ~I[1] & I[0]) |
                (~I[3] & I[2] & ~I[1] & ~I[0]) |
                (I[3] & ~I[2] & I[1] & I[0]) |
                (I[3] & I[2] & ~I[1] & I[0]);
                
    assign CB = (~I[3] & I[2] & ~I[1] & I[0]) |
                (~I[3] & I[2] & I[1] & ~I[0]) |
                (I[3] & ~I[2] & I[1] & I[0]) |
                (I[3] & I[2] & ~I[1] & ~I[0]) |
                (I[3] & I[2] & I[1] & ~I[0]) |
                (I[3] & I[2] & I[1] & I[0]);
                
    assign CC = (~I[3] & ~I[2] & I[1] & ~I[0]) |
                (I[3] & I[2] & ~I[1] & ~I[0]) |
                (I[3] & I[2] & I[1] & ~I[0]) |
                (I[3] & I[2] & I[1] & I[0]);
                
    assign CD = (~I[3] & ~I[2] & ~I[1] & I[0]) |
                (~I[3] & I[2] & ~I[1] & ~I[0]) |
                (~I[3] & I[2] & I[1] & I[0]) |
                (I[3] & ~I[2] & I[1] & ~I[0]) |
                (I[3] & I[2] & I[1] & I[0]);
    
    assign CE = (~I[3] & ~I[2] & ~I[1] & I[0]) |
                (~I[3] & ~I[2] & I[1] & I[0]) |
                (~I[3] & I[2] & ~I[1] & ~I[0]) |
                (~I[3] & I[2] & ~I[1] & I[0]) |
                (~I[3] & I[2] & I[1] & I[0]) |
                (I[3] & ~I[2] & ~I[1] & I[0]);
                
    assign CF = (~I[3] & ~I[2] & ~I[1] & I[0]) |
                (~I[3] & ~I[2] & I[1] & ~I[0]) |
                (~I[3] & ~I[2] & I[1] & I[0]) |
                (~I[3] & I[2] & I[1] & I[0]) |
                (I[3] & I[2] & ~I[1] & I[0]);
               
    assign CG = (~I[3] & ~I[2] & ~I[1] & ~I[0]) |
                (~I[3] & ~I[2] & ~I[1] & I[0]) |
                (~I[3] & I[2] & I[1] & I[0]) |
                (I[3] & I[2] & ~I[1] & ~I[0]);


endmodule
