`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/08/01 17:15:31
// Design Name: 
// Module Name: C1_test_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module C1_test_TB;

    reg [3:0] I;
    
    wire [7:0] AN;
    wire [7:0] CA;
    wire [7:0] CB;
    wire [7:0] CC;
    wire [7:0] CD;
    wire [7:0] CE;
    wire [7:0] CF;
    wire [7:0] CG;
    
    C1_test C1_test(
        .I (I[3:0]),
        .AN(AN[7:0]),
        .CA(CA),
        .CB(CB),
        .CC(CC),
        .CD(CD),
        .CE(CE),
        .CF(CF),
        .CG(CG)
    );
    
    initial begin
        I[3:0] = 4'b0000;
        #50;
        I[3:0] = 4'b0001;
        #50;
        I[3:0] = 4'b0010;
        #50;
        I[3:0] = 4'b0011;
        #50;
        I[3:0] = 4'b0100;
        #50;
        I[3:0] = 4'b0101;
        #50;
        I[3:0] = 4'b0110;
        #50;
        I[3:0] = 4'b0111;
        #50;
        I[3:0] = 4'b1000;
        #50;
        I[3:0] = 4'b1001;
        #50;
        I[3:0] = 4'b1010;
        #50;
        I[3:0] = 4'b1011;
        #50
        I[3:0] = 4'b1100;
        #50;
        I[3:0] = 4'b1101;
        #50;
        I[3:0] = 4'b1110;
        #50;
        I[3:0] = 4'b1111;
     end
         
       
endmodule
