`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/08/02 16:38:53
// Design Name: 
// Module Name: S1test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S1test(
    input CLK100M,
    input reset_SW,
    output [7:0] AN,
    output CA,
    output CB,
    output CC,
    output CD,
    output CE,
    output CF,
    output CG
    );
    
   
   wire [3:0] I; 
   reg [27:0] cnt;
   
   always @(posedge CLK100M or negedge reset_SW)begin
        if(!reset_SW)begin
            cnt[27:0] <= 28'b0;
        end else begin
            cnt[27:0] = cnt[27:0] + 28'b1;
        end
   end             
   
   assign I[3:0] = cnt[27:24];
   assign AN[7:0] = 8'b1111_1110;
    
   assign CA = (~I[3] & ~I[2] & ~I[1] & I[0]) |
                (~I[3] & I[2] & ~I[1] & ~I[0]) |
                (I[3] & ~I[2] & I[1] & I[0]) |
                (I[3] & I[2] & ~I[1] & I[0]);
                
   assign CB = (~I[3] & I[2] & ~I[1] & I[0]) |
                (~I[3] & I[2] & I[1] & ~I[0]) |
                (I[3] & ~I[2] & I[1] & I[0]) |
                (I[3] & I[2] & ~I[1] & ~I[0]) |
                (I[3] & I[2] & I[1] & ~I[0]) |
                (I[3] & I[2] & I[1] & I[0]);
                
   assign CC = (~I[3] & ~I[2] & I[1] & ~I[0]) |
                (I[3] & I[2] & ~I[1] & ~I[0]) |
                (I[3] & I[2] & I[1] & ~I[0]) |
                (I[3] & I[2] & I[1] & I[0]);
                
   assign CD = (~I[3] & ~I[2] & ~I[1] & I[0]) |
                (~I[3] & I[2] & ~I[1] & ~I[0]) |
                (~I[3] & I[2] & I[1] & I[0]) |
                (I[3] & ~I[2] & I[1] & ~I[0]) |
                (I[3] & I[2] & I[1] & I[0]);
    
   assign CE = (~I[3] & ~I[2] & ~I[1] & I[0]) |
                (~I[3] & ~I[2] & I[1] & I[0]) |
                (~I[3] & I[2] & ~I[1] & ~I[0]) |
                (~I[3] & I[2] & ~I[1] & I[0]) |
                (~I[3] & I[2] & I[1] & I[0]) |
                (I[3] & ~I[2] & ~I[1] & I[0]);
                
   assign CF = (~I[3] & ~I[2] & ~I[1] & I[0]) |
                (~I[3] & ~I[2] & I[1] & ~I[0]) |
                (~I[3] & ~I[2] & I[1] & I[0]) |
                (~I[3] & I[2] & I[1] & I[0]) |
                (I[3] & I[2] & ~I[1] & I[0]);
               
   assign CG = (~I[3] & ~I[2] & ~I[1] & ~I[0]) |
                (~I[3] & ~I[2] & ~I[1] & I[0]) |
                (~I[3] & I[2] & I[1] & I[0]) |
                (I[3] & I[2] & ~I[1] & ~I[0]); 
    
    
    
endmodule
